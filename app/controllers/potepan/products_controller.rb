class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAX_DISPLAY_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      limit(RELATED_PRODUCTS_MAX_DISPLAY_COUNT)
  end
end
