module Potepan::ProductDecorator
  def related_products
    Spree::Product.includes(:taxons, master: [:images, :default_price]).
      where(spree_products_taxons: { taxon_id: taxons }).
      where.not(spree_products_taxons: { product_id: id }).
      distinct.
      order(:id)
  end
  Spree::Product.prepend self
end
