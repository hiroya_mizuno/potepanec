require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 8, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it "正常なレスポンスを返す" do
      expect(response).to be_successful
    end

    it "正常なテンプレートを返す" do
      expect(response).to render_template :show
    end

    it "リクエストされた@productインスタンス変数が正常に割り当てられる" do
      expect(assigns(:product)).to eq (product)
    end

    it "リクエストされた関連商品@related_productsが４つある" do
      expect(assigns(:related_products).size).to eq 4 
    end
  end
end
