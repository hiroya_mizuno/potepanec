require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product)  { create :product, taxons: [taxon] }

    before do
      get :show, params: { id: taxon.id }
    end
    
    it "正常なレスポンスを返す" do
      expect(response).to be_successful
    end

    it "正常なテンプレートを返す" do
      expect(response).to render_template :show
    end

    it "リクエストされた@taxonインスタンス変数が正常に割り当てられる" do
      expect(assigns(:taxon)).to eq (taxon)
    end

    it "リクエストされた@taxonomiesインスタンス変数が正常に割り当てられる" do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    it "リクエストされた@productsインスタンス変数が正常に割り当てられる" do
      expect(assigns(:products)).to contain_exactly(product)
    end
  end
end
