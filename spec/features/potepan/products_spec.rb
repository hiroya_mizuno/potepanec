require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
  let(:other_taxon) { create(:taxon) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "「一覧ページへ戻る」リンクが正常に動作する" do
    expect(page).to have_link "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
  
  context "関連商品が５つ以上の場合に４つ表示される" do
    it "関連商品が４つ表示される" do
      within(".productsContent") do
        expect(page).to have_selector '.productBox', count: 4
      end
    end
  end

  it "関連商品リンクが正常に動作する" do
    related_products.each do |related_product|
      expect(page).to have_link related_product.name
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end

  it "関連商品の商品名が表示される" do
    expect(page).to have_content related_products.first.name
  end

  it "関連商品の価格が表示される" do
    expect(page).to have_content related_products.first.display_price
  end

  it "関連しない商品は表示されない" do
    expect(page).not_to have_content other_product.name
  end

  it "メインで表示されている商品が関連商品に表示されない" do
    within(".productsContent") do
      expect(page).not_to have_content product.name
    end
  end
end
