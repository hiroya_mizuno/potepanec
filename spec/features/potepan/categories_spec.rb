require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let!(:product) { create(:product, name: "Ruby", taxons: [taxon]) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_product) { create(:product, name: "Rails", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it "カテゴリーページが表示される" do
    expect(page).to have_content taxon.name
    expect(page).to have_content taxonomy.name
  end

  it "カテゴリーページで商品カテゴリーサイドバーが表示される" do
    within '.side-nav' do
     expect(page).to have_content taxonomy.name
     find(".dropdown-toggle").click
     expect(page).to have_link "Bags"
     click_on "Bags"
     expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  it '商品名に付与されている商品ページへのリンクが正常に動作する' do  
    expect(page).to have_link "Ruby"
    click_link "Ruby"
    expect(current_path).to eq potepan_product_path(product.id)
  end

  it "別のカテゴリーの商品は表示しない" do
    expect(page).not_to have_content "Rails"
  end
end
