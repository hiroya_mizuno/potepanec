require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe "Potepan::Products page", type: :request do
  describe "potepan/products" do
    let!(:product) { FactoryBot.create(:product, name: "Ruby", price: "$22.99", description: "description") }

    before do
      get potepan_product_path(product.id)
    end

    it "正常なレスポンスを返す" do
      expect(response).to be_successful
      expect(response).to have_http_status "200"
    end

    it "正常なテンプレートを返す" do
      expect(response).to render_template :show
    end

    it "商品名が表示される" do
      expect(response.body).to include("Ruby")
    end

    it "商品価格が表示される" do
      expect(response.body).to include("$22.99")
    end

    it "商品説明が表示される" do
      expect(response.body).to include("description")
    end
  end
end
