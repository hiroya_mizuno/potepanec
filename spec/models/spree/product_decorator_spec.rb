require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let(:taxon1) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon1]) }
    let!(:related_product) { create(:product, taxons: [taxon1]) }
    let!(:taxon2) { create(:taxon) }
    let!(:unrelated_product) { create(:product, taxons: [taxon2]) }

    it "関連する商品を取得する" do
      expect(product.related_products).to include related_product
    end

    it "関連しない商品は取得しない" do
      expect(product.related_products).not_to include unrelated_product  
    end
  end
end
