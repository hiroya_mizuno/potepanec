require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe "full_title" do
    it "returns base_title" do
      expect(full_title("")).to eq("BIGBAG Store")
    end

    it "returns base_title" do
      expect(full_title(nil)).to eq("BIGBAG Store")
    end
    
    it "returns product full_title" do
      expect(full_title('Ruby on Rails Bag')).to eq("Ruby on Rails Bag - BIGBAG Store")
    end
  end
end
